package br.com.meuspedidodesafio.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.meuspedidodesafio.R;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.presenter.detail.DetailPresenter;
import br.com.meuspedidodesafio.presenter.detail.OnDetailPresenter;
import br.com.meuspedidodesafio.presenter.detail.OnDetailView;
import br.com.meuspedidodesafio.utilities.proportion.DisplayUtil;
import br.com.meuspedidodesafio.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 08/08/2016.
 */
public class DetailFragment extends AbstractFragment implements OnDetailView {

    @Bind(R.id.photo)
    ImageView   photo;
    @Bind(R.id.title)
    TextView    title;
    @Bind(R.id.description)
    TextView    description;
    @Bind(R.id. price)
    TextView price;
    private OnMainActivityView onMainActivityView;
    private OnDetailPresenter presenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_detail, container, false);
        DisplayUtil.setLayoutParams((ViewGroup) view.findViewById(R.id.layout_detail));
        presenter   = new DetailPresenter(this);
        ButterKnife.bind(this,view);
        callDetail();
        return  view;
    }

    @Override
    public void callDetail() {
        presenter.getProductArgument(getArguments());
    }

    @Override
    public boolean returnProduct(Product product) {
        title.setText(product.getName()+"");
        description.setText(product.getDescription()+"");
        price.setText("R$ " +product.getPrice());
        String path = product.getPhoto();
        Picasso.with(getActivity())
                .load(path)
                .into(photo);

        return true;
    }
}
