package br.com.meuspedidodesafio.view.listener;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import br.com.meuspedidodesafio.utilities.enuns.ControlFrags;

public interface OnMainActivityView {

    void registrerFragment(ControlFrags frags, int layoutId, boolean addBackStack);

    void transferFragment(ControlFrags frags, int layoutId, boolean addBackStack);

    void transferFragment(ControlFrags frags, int layoutId, boolean addBackStack, Bundle bundle);

    void onBackPressed();

    Toolbar getToolbar();




}
