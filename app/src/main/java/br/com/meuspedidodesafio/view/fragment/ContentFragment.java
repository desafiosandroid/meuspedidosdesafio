package br.com.meuspedidodesafio.view.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import br.com.meuspedidodesafio.R;
import br.com.meuspedidodesafio.model.adapter.ProductAdapter;
import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.presenter.content.ContentPresenter;
import br.com.meuspedidodesafio.presenter.content.OnContentPresenter;
import br.com.meuspedidodesafio.utilities.enuns.ControlFrags;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.proportion.DisplayUtil;
import br.com.meuspedidodesafio.presenter.content.OnContentView;
import br.com.meuspedidodesafio.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;

@SuppressWarnings("ResourceAsColor")
@SuppressLint("NewApi")
public class ContentFragment extends AbstractFragment implements SwipeRefreshLayout.OnRefreshListener, OnContentView {

	private OnContentPresenter presenter;
	//recycler view
	@Bind(R.id.recyclerView)
	RecyclerView recyclerView;

	@Bind(R.id.swipe_container)
	SwipeRefreshLayout mSwipeRefreshLayout;
	//progress
	@Bind(R.id.progressBar)
	ProgressBar mProgressBar;

	ProductAdapter adapter;
	private List<Product> listProduct;
	private OnMainActivityView onMainActivityView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.content, container, false);
		DisplayUtil.setLayoutParams((ViewGroup) view.findViewById(R.id.layout_content));
		presenter = new ContentPresenter(this);
		ButterKnife.bind(this,view);
		onMainActivityView = (OnMainActivityView) getActivity();
		onMainActivityView.getToolbar().setVisibility(View.VISIBLE);
		onMainActivityView.getToolbar().setTitle("");

		//progressBar
		mProgressBar		.setVisibility(View.VISIBLE);

		//swiper
		mSwipeRefreshLayout.setOnRefreshListener(ContentFragment.this);
		mSwipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		if(MeusPedidoBO.getInstance().getListProduct() != null){
			returnListProduct(MeusPedidoBO.getInstance().getListProduct());

			for(Product p : MeusPedidoBO.getInstance().getListProduct()){
				WrapperLog.info("p " + p.getId() + " isChecked " + p.isChecked());
			}

		}else{
			//initApiRest();
			callContent(mSwipeRefreshLayout);
		}
		return view;
	}

	@Override
	public void onRefresh() {

		new Handler().postDelayed(new Runnable() {
			@Override public void run() {
				mSwipeRefreshLayout.setRefreshing(false);
				if(MeusPedidoBO.getInstance().getListProduct() != null){
					returnListProduct(MeusPedidoBO.getInstance().getListProduct());
				}else{
					//initApiRest();
					callContent(mSwipeRefreshLayout);
				}
			}
		}, 5000);

	}

	@Override
	public void callContent(SwipeRefreshLayout mSwipeRefreshLayout) {
		presenter.callContent(mSwipeRefreshLayout);
	}

	@Override
	public boolean returnListProduct(final List<Product> listProduct) {

		mProgressBar					.setVisibility(View.GONE);

		List<Product> listCustomProduct = new ArrayList<Product>();

		if(getArguments() != null){
			Category category = (Category) getArguments().getSerializable("category");
			for (Product p : listProduct){
				if(p.getCategoryId() == category.getId()){
					listCustomProduct.add(p);
				}
			}

			adapter = new ProductAdapter(listCustomProduct, getActivity());
		}else{
			adapter = new ProductAdapter(listProduct, getActivity());
		}

		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		recyclerView.setAdapter(adapter);

		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
			@Override
			public void onClick(View view, int position) {

				Product product = listProduct.get(position);
				Bundle b = new Bundle();
				b.putSerializable("product", product);
				onMainActivityView.transferFragment(ControlFrags.DETAIL, R.id.content, true, b);
			}

			@Override
			public void onLongClick(View view, int position) {

			}
		}));

		return true;
	}

	//RecyclerView
	public interface ClickListener {
		void onClick(View view, int position);

		void onLongClick(View view, int position);
	}

	public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

		private GestureDetector gestureDetector;
		private ContentFragment.ClickListener clickListener;

		public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ContentFragment.ClickListener clickListener) {
			this.clickListener = clickListener;
			gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
				@Override
				public boolean onSingleTapUp(MotionEvent e) {
					return true;
				}

				@Override
				public void onLongPress(MotionEvent e) {
					View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
					if (child != null && clickListener != null) {
						clickListener.onLongClick(child, recyclerView.getChildPosition(child));
					}
				}
			});
		}

		@Override
		public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

			View child = rv.findChildViewUnder(e.getX(), e.getY());
			if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
				clickListener.onClick(child, rv.getChildPosition(child));
			}
			return false;
		}

		@Override
		public void onTouchEvent(RecyclerView rv, MotionEvent e) {
		}

		@Override
		public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

		}
	}
}
