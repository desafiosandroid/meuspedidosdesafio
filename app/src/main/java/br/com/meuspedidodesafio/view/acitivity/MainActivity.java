package br.com.meuspedidodesafio.view.acitivity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import br.com.meuspedidodesafio.R;
import br.com.meuspedidodesafio.utilities.application.MeusPedidoApplication;
import br.com.meuspedidodesafio.utilities.enuns.ControlFrags;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.proportion.DisplayUtil;
import br.com.meuspedidodesafio.view.fragment.NavigationDrawerFragment;
import br.com.meuspedidodesafio.view.listener.OnMainActivityView;

public class MainActivity extends AbstractActivity implements OnMainActivityView {

    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    public static boolean isOpen = true;

    @SuppressWarnings("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_toolbar);
        DisplayUtil.setLayoutParams((ViewGroup) findViewById(R.id.main));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            addFragment(ControlFrags.CONTENT, R.id.content, false);

        }

        toolbar.setTitle("");
        toolbar.setTitleTextColor(R.color.colorPrimary);
        MeusPedidoBO.getInstance().setApplication((MeusPedidoApplication) getApplication());
        drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        drawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout)findViewById(R.id.layout_navigation), null);
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @SuppressWarnings("WrongConstant")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.drawer) {

            if(isOpen){
                NavigationDrawerFragment.drawerLayout.openDrawer(Gravity.END);
                isOpen = false;
            }else{
                NavigationDrawerFragment.drawerLayout.closeDrawers();
                isOpen = true;
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * method to call the fragment
     */
    @Override
    public void registrerFragment(ControlFrags frags, int layoutId, boolean addBackStack) {
        registrerFragment(frags, layoutId, addBackStack);
    }

    /**
     * method to transfer the fragment
     */

    @Override
    public void transferFragment(ControlFrags frags, int layoutId, boolean addBackStack) {
        replaceFragment(frags, layoutId, addBackStack);
    }

    /**
     * method to send bundle to another fragment
     */

    @Override
    public void transferFragment(ControlFrags frags, int layoutId, boolean addBackStack, Bundle bundle) {
        replaceFragment(frags, layoutId, addBackStack, bundle);
    }

}
