package br.com.meuspedidodesafio.view.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.meuspedidodesafio.R;
import br.com.meuspedidodesafio.model.adapter.CategoryAdapter;
import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.presenter.drawer.NavigationPresenter;
import br.com.meuspedidodesafio.presenter.drawer.OnNavigationPresenter;
import br.com.meuspedidodesafio.presenter.drawer.OnNavigationView;
import br.com.meuspedidodesafio.utilities.enuns.ControlFrags;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.proportion.DisplayUtil;
import br.com.meuspedidodesafio.utilities.service.RestApi;
import br.com.meuspedidodesafio.view.acitivity.MainActivity;
import br.com.meuspedidodesafio.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

@SuppressLint("NewApi")
public class NavigationDrawerFragment extends AbstractFragment implements OnNavigationView {

	private static final String PREF_FILE_NAME = "nightpref";
	private static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";

	private ActionBarDrawerToggle mDrawerToggle;
	public static DrawerLayout drawerLayout;

	private OnNavigationPresenter presenter;

	private View contentView;

	private boolean mUserLearnerDrawer;
	private boolean mFromSaveInstanceState;

	@Bind(R.id.categorys)
	RelativeLayout	categorys;
	//recycler view

	@Bind(R.id.recyclerViewNavigation)
	RecyclerView recyclerView;

	@Bind(R.id.navigation_drawer_layout)
	RelativeLayout		layout;

	CategoryAdapter			  adapter;
	private List<Category>    listCategory;
	private OnMainActivityView onMainActivityView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mUserLearnerDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));
		if(savedInstanceState != null){
			mFromSaveInstanceState = true;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.navigation_drawer, container, false);
		ButterKnife.bind(this,view);
		presenter	= new NavigationPresenter(this);
		onMainActivityView      = (OnMainActivityView) getActivity();
		layout.setOnClickListener(null);

		callNavigation();
		return view;
	}

	@Override
	public void resultListCategory(final List<Category> listCategory) {

		adapter = new CategoryAdapter(listCategory, getActivity());
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		recyclerView.setAdapter(adapter);

		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
			@Override
			public void onClick(View view, int position) {

				Category category = listCategory.get(position);
				Bundle b = new Bundle();
				b.putSerializable("category", category);
				onMainActivityView.transferFragment(ControlFrags.CONTENT, R.id.content, false, b);
				drawerLayout.closeDrawers();
				MainActivity.isOpen = true;
			}

			@Override
			public void onLongClick(View view, int position) {

			}
		}));

	}

	@Override
	public void callNavigation() {
		presenter.callNavigation();
	}

	//RecyclerView
	public interface ClickListener {
		void onClick(View view, int position);

		void onLongClick(View view, int position);
	}

	public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

		private GestureDetector gestureDetector;
		private NavigationDrawerFragment.ClickListener clickListener;

		public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final NavigationDrawerFragment.ClickListener clickListener) {
			this.clickListener = clickListener;
			gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
				@Override
				public boolean onSingleTapUp(MotionEvent e) {
					return true;
				}

				@Override
				public void onLongPress(MotionEvent e) {
					View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
					if (child != null && clickListener != null) {
						clickListener.onLongClick(child, recyclerView.getChildPosition(child));
					}
				}
			});
		}

		@Override
		public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

			View child = rv.findChildViewUnder(e.getX(), e.getY());
			if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
				clickListener.onClick(child, rv.getChildPosition(child));
			}
			return false;
		}

		@Override
		public void onTouchEvent(RecyclerView rv, MotionEvent e) {
		}

		@Override
		public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

		}
	}

	public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {

		contentView = getActivity().findViewById(fragmentId);
		this.drawerLayout = drawerLayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if(!mUserLearnerDrawer){
					mUserLearnerDrawer = true;
					saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnerDrawer+"");
				}
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				if(slideOffset < 0.6 ){
					//toolbar.setAlpha(1-slideOffset);
				}

			}
		};
		if(!mUserLearnerDrawer && !mFromSaveInstanceState){
			mDrawerToggle.onDrawerOpened(contentView);
		}
		this.drawerLayout.setDrawerListener(mDrawerToggle);
		this.drawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});
	}

	public static void saveToPreferences(Context context, String preferenceName, String preferenceValue){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(preferenceName, preferenceValue);
		editor.commit();

	}

	public static String readFromPreferences(Context context, String preferenceName, String preferenceValue){
		SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString(preferenceName, preferenceValue);
	}

	@OnClick(R.id.categorys)
	protected void onCategorys() {
		onMainActivityView.transferFragment(ControlFrags.CONTENT, R.id.content, false);
		this.drawerLayout.closeDrawers();
		MainActivity.isOpen = true;
	}
}
