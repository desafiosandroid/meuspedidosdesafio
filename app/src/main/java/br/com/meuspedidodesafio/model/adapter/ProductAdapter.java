package br.com.meuspedidodesafio.model.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.meuspedidodesafio.R;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.proportion.DisplayUtil;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by Enzo Teles on 17/06/2016.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.Holder> {

    private List<Product> mListProduct;
    private Activity activity;

    public ProductAdapter(List<Product> mListProduct, Activity activity) {
        this.mListProduct           = mListProduct;
        this.activity               = activity;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView               = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_card, parent, false);
        DisplayUtil.setLayoutParams((ViewGroup) itemView.findViewById(R.id.item_product));
        return new Holder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(Holder holder, int position) {

        final Product product                 = mListProduct.get(position);
        String path                     = product.getPhoto()+"";
        holder.name                     .setText(product.getName()+"");
        holder.price                    .setText("R$ " +product.getPrice());
        Picasso.with(activity)
                .load(path)
                .into(holder.photo);


        if(product.isChecked() == false){
            holder.favorite.setBackgroundResource(R.drawable.checkbox_favorites);
        }else{
            holder.favorite.setBackgroundResource(R.drawable.checkbox_favorites_unpress);
        }

        holder.favorite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Product pp = new Product();

                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                      for(Product p : MeusPedidoBO.getInstance().getListProduct()){
                        if(product.getId() == p.getId()){
                            product.setChecked(true);
                            pp = product;
                            MeusPedidoBO.getInstance().updateProduct(pp);
                        }
                      }
                }else{
                    for(Product p : MeusPedidoBO.getInstance().getListProduct()){
                        if(product.getId() == p.getId()){
                            product.setChecked(false);
                            pp = product;
                            MeusPedidoBO.getInstance().updateProduct(pp);
                        }
                    }
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mListProduct.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.name)
        TextView    name;
        @Bind(R.id.price)
        TextView    price;
        @Bind(R.id.photo)
        ImageView   photo;
        @Bind(R.id.favorite)
        CheckBox    favorite;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
