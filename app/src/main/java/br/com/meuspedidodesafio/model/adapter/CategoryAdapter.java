package br.com.meuspedidodesafio.model.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.meuspedidodesafio.R;
import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.utilities.proportion.DisplayUtil;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by Enzo Teles on 17/06/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Holder> {

    private List<Category> mListCategory;
    private Activity activity;

    public CategoryAdapter(List<Category> mListCategory, Activity activity) {
        this.mListCategory           = mListCategory;
        this.activity               = activity;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView               = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        DisplayUtil.setLayoutParams((ViewGroup) itemView.findViewById(R.id.layout_category));
        return new Holder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(Holder holder, int position) {

        Category category                 = mListCategory.get(position);
        holder.category                   .setText(category.getName()+"");
    }

    @Override
    public int getItemCount() {
        return mListCategory.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.category)
        Button   category;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
