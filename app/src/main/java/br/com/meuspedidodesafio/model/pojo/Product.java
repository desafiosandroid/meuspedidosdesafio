
package br.com.meuspedidodesafio.model.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "product")
@Generated("org.jsonschema2pojo")
public class Product extends GenericEntity implements Serializable{

    @DatabaseField(columnName = "name")
    @SerializedName("name")
    @Expose
    private String name;
    @DatabaseField(columnName = "description")
    @SerializedName("description")
    @Expose
    private String description;
    @DatabaseField(columnName = "photo")
    @SerializedName("photo")
    @Expose
    private String photo;
    @DatabaseField(columnName = "price")
    @SerializedName("price")
    @Expose
    private String price;
    @DatabaseField(columnName = "category_id")
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;

    @DatabaseField(columnName = "isChecked")
    private boolean isChecked;

    public Product() {
    }

    public Product(String name, String description, String photo, Integer categoryId, boolean isChecked) {
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.price = price;
        this.categoryId = categoryId;
        this.isChecked = isChecked;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * 
     * @param photo
     *     The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The categoryId
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * 
     * @param categoryId
     *     The category_id
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
