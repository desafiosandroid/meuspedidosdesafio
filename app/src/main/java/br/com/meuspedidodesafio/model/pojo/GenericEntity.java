package br.com.meuspedidodesafio.model.pojo;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;

public class GenericEntity implements Serializable{

	/**
	 * SERIAL
	 */
	private static final long serialVersionUID = 1L;
	@DatabaseField(generatedId=true, columnName="id")
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	

}
