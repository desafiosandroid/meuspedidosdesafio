package br.com.meuspedidodesafio.utilities.enuns;

import br.com.meuspedidodesafio.view.fragment.AbstractFragment;
import br.com.meuspedidodesafio.view.fragment.ContentFragment;
import br.com.meuspedidodesafio.view.fragment.DetailFragment;

/**
 * Here are all enums that will be used in the application, along with its keys and values
 * */
public enum ControlFrags {

	DETAIL	     			("detail", DetailFragment.class),
	CONTENT	     			("content", ContentFragment.class);

	
	private String name;
	private Class<? extends AbstractFragment> classFrag;
	
	private ControlFrags(final String name, Class<? extends AbstractFragment> classFrag) {
		this.name = name;
		this.classFrag = classFrag;
	}



	public String getName() {
		return name;
	}
	public Class<? extends AbstractFragment> getClassFrag() {
		return classFrag;
	}
}
