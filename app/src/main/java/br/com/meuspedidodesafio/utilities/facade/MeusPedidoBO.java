package br.com.meuspedidodesafio.utilities.facade;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.utilities.application.MeusPedidoApplication;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.service.RestApi;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class MeusPedidoBO {

	private static MeusPedidoBO instance;
	private static final Object SYNCOBJECT = new Object();
	private String access_token;
	private MeusPedidoApplication application;

	// variable persistence data
	private transient Dao<Product, Integer> productDao;
	private transient Dao<Category, Integer> categoryDao;


	/**
	 * MarkerBO is a Singleton
	 */
	public static MeusPedidoBO getInstance() {

		synchronized (SYNCOBJECT) {
			if (instance == null) {
				instance = new MeusPedidoBO();
			}
		}

		return instance;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	/**
	 * methot to return the head of the APIRest
	 * */
	public static RestAdapter getRestAdapter() {

		final Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(RestApi.REST_ENDPOINT)
				.setConverter(new GsonConverter(gson))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.build();

		return restAdapter;
	}

	// #############################################################
	// 							PRODUCT
	// #############################################################

	/**
	 * method to add a product in base
	 * */
	public boolean insertProduct(final Product product) {

		try {
			productDao = getApplication().getProductDao();
			productDao.create(product);
		} catch (SQLException e) {
			WrapperLog.error("error");
		}

		return true;
	}

	/**
	 * method to list a product
	 * */
	public List<Product> getListProduct() {
		List<Product> products = null;

		try {
			productDao = getApplication().getProductDao();
			products = productDao.queryForAll();
			if (!products.isEmpty()) {
				return products;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return products;

	}

	/**
	 * method to delete a product in base
	 * */
	public boolean deleteProduct(final Product product) {

		try {
			productDao.delete(product);
			return true;
		} catch (SQLException e) {
			WrapperLog.error("error");
			return false;
		}
	}

	/**
	 * method to update a product in base
	 * */
	public boolean updateProduct(final Product product) {

		WrapperLog.info("product "+ product.getName());

		try {
			productDao.createOrUpdate(product);
			WrapperLog.info("product: " + product.getName());
			return true;
		} catch (SQLException e) {
			WrapperLog.error("error" + e.getMessage());
			return false;
		}
	}

	public MeusPedidoApplication getApplication() {
		return application;
	}

	public void setApplication(MeusPedidoApplication application) {
		this.application = application;
	}
}
