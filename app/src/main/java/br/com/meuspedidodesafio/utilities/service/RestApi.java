package br.com.meuspedidodesafio.utilities.service;

import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 21/06/2016.
 */
public interface RestApi {

    String REST_ENDPOINT = "https://gist.github.com/";

    @GET("/ronanrodrigo/e84d0d969613fd0ef8f9fd08546f7155/raw/a0611f7e765fa2b745ad9a897296e082a3987f61/categories.json")
    void getCategory(Callback<List<Category>> cb);

    @GET("/ronanrodrigo/b95b75cfddc6b1cb601d7f806859e1dc/raw/dc973df65664f6997eeba30158d838c4b716204c/products.json")
    void getProduct(Callback<List<Product>> cb);
}
