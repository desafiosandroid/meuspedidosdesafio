package br.com.meuspedidodesafio.utilities.application;

import java.sql.SQLException;

import android.app.Application;

import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.persistence.DataBaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

public class MeusPedidoApplication extends Application {

	private DataBaseHelper databaseHelper = null;

	private Dao<Product, Integer> productDAO = null;
	private Dao<Category, Integer> categoryDAO = null;

	@Override
	public void onCreate() {
		super.onCreate();
		databaseHelper = new DataBaseHelper(this);
	}

	public Dao<Product, Integer> getProductDao() throws SQLException {
		if (productDAO == null) {
			productDAO = databaseHelper.getDao(Product.class);
		}
		return productDAO;
	}

	public Dao<Category, Integer> getCategoryDao() throws SQLException {
		if (categoryDAO == null) {
			categoryDAO = databaseHelper.getDao(Category.class);
		}
		return categoryDAO;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

}
