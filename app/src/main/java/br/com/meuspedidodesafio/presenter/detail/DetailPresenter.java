package br.com.meuspedidodesafio.presenter.detail;

import android.os.Bundle;

import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.view.fragment.DetailFragment;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public class DetailPresenter implements OnDetailPresenter {

    private OnDetailView onDetailView;

    public DetailPresenter(OnDetailView onDetailView) {
        this.onDetailView = onDetailView;
    }

    @Override
    public void getProductArgument(Bundle arguments) {
        if(arguments != null) {
            Product product = (Product) arguments.getSerializable("product");
            onDetailView.returnProduct(product);
        }
    }
}
