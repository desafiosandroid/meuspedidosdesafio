package br.com.meuspedidodesafio.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.service.RestApi;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public class ContentIteractor implements OnContentIteractor {

    List<Product> listProduct;

    @Override
    public void callContent(final OnContentCallback callback, final SwipeRefreshLayout mSwipeRefreshLayout) {

        listProduct               = new ArrayList<Product>();

        RestAdapter restAdapter = MeusPedidoBO.getRestAdapter();
        restAdapter.create(RestApi.class).getProduct(new Callback<List<Product>>() {
            @Override
            public void success(List<Product> products, Response response) {
                for (Product p : products){
                    listProduct.add(p);
                    WrapperLog.info("Product ============= " + p.isChecked());
                    MeusPedidoBO.getInstance().insertProduct(p);
                }
                callback.returnListProduct(listProduct);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.error("Error " + error.getMessage());
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
