package br.com.meuspedidodesafio.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public interface OnContentIteractor {
    void callContent(OnContentCallback callback, SwipeRefreshLayout mSwipeRefreshLayout);
}
