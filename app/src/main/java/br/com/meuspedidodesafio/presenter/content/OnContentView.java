package br.com.meuspedidodesafio.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Product;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public interface OnContentView {

    void callContent(SwipeRefreshLayout mSwipeRefreshLayout);

    boolean returnListProduct(List<Product> listProduct);
}
