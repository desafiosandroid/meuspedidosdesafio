package br.com.meuspedidodesafio.presenter.drawer;

import java.util.ArrayList;
import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.service.RestApi;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public class NavigationIteractor implements OnNavigationIteractor {

    List<Category> listCategory;

    @Override
    public void callNavigation(final OnNavigationCallback callback) {
        listCategory               = new ArrayList<Category>();

        RestAdapter restAdapter = MeusPedidoBO.getRestAdapter();
        restAdapter.create(RestApi.class).getCategory(new Callback<List<Category>>() {
            @Override
            public void success(List<Category> categories, Response response) {
                for (Category c : categories){
                    listCategory.add(c);
                    WrapperLog.info("categorias " + c.getName());
                }
                callback.resultListCategory(listCategory);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.info("error " + error.getMessage());
            }
        });
    }
}
