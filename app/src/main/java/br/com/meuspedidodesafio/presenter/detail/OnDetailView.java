package br.com.meuspedidodesafio.presenter.detail;

import br.com.meuspedidodesafio.model.pojo.Product;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public interface OnDetailView {
    void callDetail();
    boolean returnProduct(Product product);
}
