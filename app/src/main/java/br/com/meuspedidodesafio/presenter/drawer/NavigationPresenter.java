package br.com.meuspedidodesafio.presenter.drawer;

import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.view.fragment.NavigationDrawerFragment;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public class NavigationPresenter implements OnNavigationPresenter, OnNavigationCallback {

    private OnNavigationIteractor iteractor;
    private OnNavigationView onNavigationView;

    public NavigationPresenter(OnNavigationView onNavigationView) {
        this.onNavigationView   = onNavigationView;
        this.iteractor = new NavigationIteractor();
    }

    @Override
    public void callNavigation() {
        iteractor.callNavigation(this);
    }

    @Override
    public boolean resultListCategory(List<Category> listCategory) {
        onNavigationView.resultListCategory(listCategory);
        return true;
    }
}
