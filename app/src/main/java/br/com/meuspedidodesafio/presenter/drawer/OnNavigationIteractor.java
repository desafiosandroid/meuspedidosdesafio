package br.com.meuspedidodesafio.presenter.drawer;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public interface OnNavigationIteractor {
    void callNavigation(OnNavigationCallback callback);
}
