package br.com.meuspedidodesafio.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Product;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public class ContentPresenter implements OnContentPresenter, OnContentCallback {
    private OnContentView onContentView;
    private OnContentIteractor iteractor;

    public ContentPresenter(OnContentView onContentView) {
        this.onContentView = onContentView;
        this.iteractor  = new ContentIteractor();
    }



    @Override
    public void callContent(SwipeRefreshLayout mSwipeRefreshLayout) {
        iteractor.callContent(this, mSwipeRefreshLayout);
    }

    @Override
    public boolean returnListProduct(List<Product> listProduct) {
        onContentView.returnListProduct(listProduct);
        return  true;
    }
}
