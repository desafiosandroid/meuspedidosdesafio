package br.com.meuspedidodesafio.presenter.detail;

import android.os.Bundle;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public interface OnDetailPresenter {
    void getProductArgument(Bundle arguments);
}
