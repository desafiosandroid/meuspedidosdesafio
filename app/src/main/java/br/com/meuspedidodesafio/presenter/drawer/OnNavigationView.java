package br.com.meuspedidodesafio.presenter.drawer;

import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Category;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 09/08/2016.
 */
public interface OnNavigationView {
    void callNavigation();
    void resultListCategory(List<Category> listCategory);
}
