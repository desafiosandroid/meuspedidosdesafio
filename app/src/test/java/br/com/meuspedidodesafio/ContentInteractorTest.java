package br.com.meuspedidodesafio;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.presenter.content.OnContentCallback;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.service.RestApi;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 10/08/2016.
 */
public class ContentInteractorTest extends TestCase {

    List<Product> listProduct;
    OnContentCallback contentCallback;
    public void testReturnListProduct(){
        listProduct               = new ArrayList<Product>();

        RestAdapter restAdapter = MeusPedidoBO.getRestAdapter();
        restAdapter.create(RestApi.class).getProduct(new Callback<List<Product>>() {
            @Override
            public void success(List<Product> products, Response response) {
                for (Product p : products){
                    listProduct.add(p);
                    MeusPedidoBO.getInstance().insertProduct(p);
                }
                assertNotNull(listProduct);
                boolean result = contentCallback.returnListProduct(listProduct);
                assertTrue(result);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.error("Error " + error.getMessage());
            }
        });
    }
}
