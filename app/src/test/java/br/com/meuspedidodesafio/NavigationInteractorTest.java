package br.com.meuspedidodesafio;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import br.com.meuspedidodesafio.model.pojo.Category;
import br.com.meuspedidodesafio.model.pojo.Product;
import br.com.meuspedidodesafio.presenter.drawer.OnNavigationCallback;
import br.com.meuspedidodesafio.utilities.facade.MeusPedidoBO;
import br.com.meuspedidodesafio.utilities.log.WrapperLog;
import br.com.meuspedidodesafio.utilities.service.RestApi;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 10/08/2016.
 */
public class NavigationInteractorTest extends TestCase {

    List<Category> listCategory;
    OnNavigationCallback callback;
    public void testReturnListCategory(){
        listCategory               = new ArrayList<Category>();

        RestAdapter restAdapter = MeusPedidoBO.getRestAdapter();
        restAdapter.create(RestApi.class).getCategory(new Callback<List<Category>>() {
            @Override
            public void success(List<Category> categories, Response response) {
                for (Category c : categories){
                    listCategory.add(c);
                    WrapperLog.info("categorias " + c.getName());
                }
                assertNotNull(listCategory);
                boolean result = callback.resultListCategory(listCategory);
                assertTrue(result);
            }

            @Override
            public void failure(RetrofitError error) {
                WrapperLog.info("error " + error.getMessage());
            }
        });
    }
}
