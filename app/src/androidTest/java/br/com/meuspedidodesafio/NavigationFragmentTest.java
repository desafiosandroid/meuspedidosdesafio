package br.com.meuspedidodesafio;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import br.com.meuspedidodesafio.view.acitivity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 11/08/2016.
 */
public class NavigationFragmentTest {

    // Preferred JUnit 4 mechanism of specifying the activity to be launched before each test
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void clickOnItemRecyclerViewNavigation() {
        // Click on the RecyclerView item at position 1
        onView(withId(R.id.recyclerViewNavigation)).perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));
    }
}
